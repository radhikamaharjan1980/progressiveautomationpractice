package PageObjectModel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testBase.BasePage;

public class DriverStatusPage extends BasePage {
	@FindBy(xpath="//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_F']")
	WebElement select_Gender;
	
	
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']")
	WebElement dropdown_MaritalStatus;
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']//option[contains(text(),'Single')]")
	WebElement select_MaritalStatus;

	
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']")
	WebElement dropdown_HighestLevelOfEducation;
    @FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']//option[contains(text(),'High school diploma or GED')]")
	WebElement select_HighestLevelOfEducation;
	
	
	@FindBy(xpath="//select[@analytics-id='DriversAddPniDetails_EmploymentStatus']")
	WebElement dropdown_EmploymentStatus;
	@FindBy(xpath="//select[@analytics-id='DriversAddPniDetails_EmploymentStatus']//option[contains(text(),'Not working/Other')]")
	WebElement select_EmploymentStatus;
	
	
	
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']")
	WebElement dropdown_primaryresidence;
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']//option[contains(text(),'Own home')]")
	WebElement select_primaryresidence;
	
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']")
	WebElement dropdown_MovingMonth;
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']//option[contains(text(),'No')]")
	WebElement select_MovingMonth;
	
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_LicenseStatus']")
	WebElement dropdown_Licencestatus;
	@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_LicenseStatus']//option[contains(text(),'Permit')]")
	WebElement select_Licencestatus;
	
	@FindBy(xpath="//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']")
	WebElement select_AccidentClaim;
	
	@FindBy(xpath="//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']")
	WebElement select_Ticketviolation;
	@FindBy(xpath="//loading-button[@class='blue']")
	WebElement click_continue;
	
	
	
	public DriverStatusPage() {
		PageFactory.initElements(driver, this);
		}
		
		public void DriverInfo() throws Exception {
			
			Thread.sleep(4000);
			select_Gender.click();
			Thread.sleep(4000);
			dropdown_MaritalStatus.click();
			Thread.sleep(4000);
			select_MaritalStatus.click();
			Thread.sleep(4000);
			dropdown_HighestLevelOfEducation.click();
			Thread.sleep(4000);
			select_HighestLevelOfEducation.click();
			Thread.sleep(4000);
			dropdown_EmploymentStatus.click();
			Thread.sleep(4000);
			select_EmploymentStatus.click();
			Thread.sleep(4000);
			dropdown_primaryresidence.click();
			Thread.sleep(4000);
			select_primaryresidence.click();
			Thread.sleep(4000);
			dropdown_MovingMonth.click();
			Thread.sleep(4000);
			select_MovingMonth.click();
			Thread.sleep(4000);
			dropdown_Licencestatus.click();
			Thread.sleep(4000);
			select_Licencestatus.click();
			Thread.sleep(4000);
			select_AccidentClaim.click();
			Thread.sleep(4000);
			select_Ticketviolation.click();
			Thread.sleep(4000);
			click_continue.click();
			Thread.sleep(5000);
			
		}
}
		

			
			
	
			
			
	
	

	

	

	

	

	

	
	

	
	
	

