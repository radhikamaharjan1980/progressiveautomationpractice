package PageObjectModel;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.json.StaticInitializerCoercer;
import org.testng.annotations.AfterTest;

public class GooglePage {
	public static WebDriver driver;
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\s_mah\\Desktop\\New folder (5)\\chromedriver.exe");
		
		 driver = new ChromeDriver();
		
		driver.get("https://www.google.com/");
	    driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "-----was Launched");
	
		
		WebElement SearchField = driver.findElement(By.xpath("//input[@name='q']"));
		if (SearchField.isDisplayed()) {
			System.out.println("Sucess---Search Field is Displayed as Expected");
		} else {
			System.out.println("Failed---Search Field is not displayed as expected");
		}
			
			SearchField.sendKeys("what is selenium");
			SearchField.sendKeys(Keys.RETURN);
			
			String pageSource = driver.getPageSource();
			
			if (pageSource.contains("Selenium")){
				System.out.println("Sucess--Valid search were displayed");
			} else {
				System.out.println("Failed--Invalidsearch result were displayed");
				
			}
			@AfterTest
		public static void closeTest() {
				//close browser
				driver.close();
				driver.quit();
				System.out.println("TestCompleted");
				
				
			}
			
			
		}
}
	


