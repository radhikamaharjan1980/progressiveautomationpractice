package PageObjectModel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testBase.BasePage;

public class InsurancePage extends BasePage {
	@FindBy(xpath ="//input[@name='viewMcodel_embedded_questions_list_InsuranceToday']")
	WebElement select_radioBtn;
	
	@FindBy(xpath="//select[@idFinalDetailsEdit_embedded_questions_list_RecentAutoInsuranceCompanyPeriod']")
	WebElement dropdown_currentcompany;
	@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_RecentAutoInsuranceCompanyPeriod']//option[contains(text(),'Less than 1 year')]")
	WebElement select_currentcompany;
	
	@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_BodilyInjuryLimits']")
	WebElement dropdown_injuredlimit;
	@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_BodilyInjuryLimits']//option[contains(text),'State Min')]")
	WebElement select_injurylimits;
	
	@FindBy(xpath="//input[@id='FinalDetailsEdit_embedded_questions_list_OtherPolicies_N']")
	WebElement select_button;
	
	@FindBy(xpath="//input[@id='FinalDetailsEdit_embedded_questions_list_PriorProgressive_N']")
	WebElement select_Nobutton;
	
	@FindBy(xpath="//input[@id='FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress']")
	WebElement email_address;
			
	@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_TotalResidents']")
	WebElement dropdown_totalresidence;
	@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_TotalResidents']//option[contains(text(),'4')]")
	WebElement select_totalresidence;
	
	@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_TotalPipClaimsCount']")
	WebElement select_totalclaims;
	
	@FindBy(xpath="//loading-button[@class='blue']//button")
	WebElement click_continue;
	
	
	
	public InsurancePage () {
		PageFactory.initElements(driver, this);
	}
	public void InsuranceInfo() throws Exception {
		Thread.sleep(5000);
		select_radioBtn.click();
		Thread.sleep(4000);
		dropdown_currentcompany.click();
		Thread.sleep(3000);
		select_currentcompany.click();
		Thread.sleep(5000);
		dropdown_injuredlimit.click();
		Thread.sleep(4000);
		select_injurylimits.click();
		Thread.sleep(5000);
		select_button.click();
		Thread.sleep(5000);
		select_Nobutton.click();
		email_address.sendKeys("samdam@gmail.com");
		Thread.sleep(4000);
		dropdown_totalresidence.click();
		Thread.sleep(4000);
		select_totalresidence.sendKeys("4");
		Thread.sleep(4000);
		
		
		select_totalclaims.sendKeys("0");
		Thread.sleep(4000);
		click_continue.click();
		Thread.sleep(4000);





		


	}



	

}
