package PageObjectModel;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testBase.BasePage;

public class NameListPage extends BasePage{
	
	@FindBy(xpath ="//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName']")
	WebElement first_name;
	@FindBy(xpath ="//input[@id='NameAndAddressEdit_embedded_questions_list_LastName']")
	WebElement last_name;
	@FindBy(xpath ="//input[@id ='NameAndAddressEdit_embedded_questions_list_MiddleInitial']")
	WebElement middle_name;
	@FindBy(xpath ="//input[@name='NameAndAddressEdit_embedded_questions_list_DateOfBirth']")
	WebElement date_of_birth;
	@FindBy(xpath ="//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress']")
	WebElement street_number;
	@FindBy(xpath = "//input[@name='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']")
	WebElement apt_unit;
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_City']")
	WebElement city_name;
	@FindBy(xpath = "//input[@name='NameAndAddressEdit_embedded_questions_list_ZipCode']")
	WebElement zip_code;
	@FindBy (xpath = "//loading-button[@class='blue']")
	WebElement click_StartMyQuote;
	


	public NameListPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void personalInfo() throws Exception {
		first_name.sendKeys("Nancy");
		
		
		last_name.sendKeys("Policy");
		

		middle_name.sendKeys("Lal");
		Thread.sleep(5000);

	

		date_of_birth.sendKeys("09/14/1970");
		Thread.sleep(5000);
		
		
		street_number.sendKeys(Keys.CLEAR);
		Thread.sleep(3000);
		street_number.sendKeys(Keys.TAB);
		Thread.sleep(3000);

		street_number.sendKeys("1040 Heartz Dr");
		Thread.sleep(3000);

	

		apt_unit.sendKeys("220");
		Thread.sleep(4000);
	

		city_name.sendKeys("coppell");
		Thread.sleep(3000);

		//zip_code.sendKeys("75019");
		
		click_StartMyQuote.click();
		

	}
	
	
	}


	


 


