package PageObjectModel;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testBase.BasePage;

public class VehiclePage extends BasePage {
	
	@FindBy(xpath ="//ul[@class='ng-star-inserted']//li[2]")
	WebElement year_name;	
	@FindBy(xpath ="//ul//li[contains(text(),'BMW')]")
	WebElement make_name;
	@FindBy(xpath ="//list-input[@id='VehiclesNew_embedded_questions_list_Model']//ul//li[16]")
	WebElement model_name;
	
	@FindBy(xpath="//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']")
	WebElement dropdown_primaryUse;
	@FindBy(xpath="//select-input//select//option[2]")
	WebElement select_primaryUse;
	
    
	@FindBy(xpath="//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']")
	WebElement dropdown_ownOrLease;
	@FindBy(xpath ="//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']//option[2]")
	WebElement select_ownOrLease;
	
	@FindBy(xpath="//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']")
	WebElement dropdown_vehicleDuration;
	@FindBy(xpath ="//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']//option[4]")
	WebElement select_vehicleDuration;
	
	@FindBy(xpath="//loading-button[@class='blue']//button[contains(text(),'Done')]")
	WebElement click_done;
	@FindBy(xpath="//loading-button[@class='blue']")
	WebElement click_continue;
	
	
	
	
	
	
	public VehiclePage () {
		PageFactory.initElements(driver, this);
	}
	public void VehicleInfo() throws Exception {
		Thread.sleep(5000);
		year_name.click();
		Thread.sleep(2000);
		//year_name.sendKeys(Keys.TAB);
		//Thread.sleep(2000);
		
		make_name.click();
		Thread.sleep(5000);
		model_name.click();
		Thread.sleep(3000);                                                                                                                                
		dropdown_primaryUse.click();
		select_primaryUse.click();
		dropdown_ownOrLease.click();
		select_ownOrLease.click();
		dropdown_vehicleDuration.click();
		select_vehicleDuration.click();
		click_done.click();
		
		Thread.sleep(3000);
		click_continue.click();
		
	}
	
}
		
		
		
		
		
		
	

	
	


	
 
	
	


