package PageObjectModel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testBase.BasePage;

public class snapShotPage extends BasePage {
	
	@FindBy(xpath="//input[@id='SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_N']")
	WebElement click_enrollment;
	
	
	public snapShotPage () {
		PageFactory.initElements(driver, this);
	}
	public void snapShotInfo () throws Exception {
		Thread.sleep(5000);
		click_enrollment.click();
	}
		

}

