package PageObjectModel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testBase.BasePage;

public class zipCodePage extends BasePage {
	
	@FindBy(xpath ="//input[@id='zipCode_overlay']")
	WebElement enterzipcode;
	
	@FindBy(xpath = "//input[@id='qsButton_overlay']")
	WebElement clickquote;
	
	public zipCodePage () {
		PageFactory.initElements(driver, this);
	}
	
	public void zipcode() throws InterruptedException {
		Thread.sleep(2000);
		enterzipcode.sendKeys("75019");
		Thread.sleep(2000);
		clickquote.click();
	}
	

}
