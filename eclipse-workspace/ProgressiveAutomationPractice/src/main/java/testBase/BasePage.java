package testBase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import PageObjectModel.DriverStatusPage;
import PageObjectModel.HomePage;
import PageObjectModel.InsurancePage;
import PageObjectModel.NameListPage;
import PageObjectModel.VehiclePage;
import PageObjectModel.snapShotPage;
import PageObjectModel.zipCodePage;

public class BasePage {
	
	protected static WebDriver driver;
	public static HomePage homeP; 
	
	public static zipCodePage zipquote ;
	
	public static NameListPage nameP ;
	
	public static VehiclePage vehicleP ;
	
	public static DriverStatusPage dStatusP;
	
	public static InsurancePage insuranceP;
	
	public static snapShotPage snapSP ;
	
	
	
	public static void invokeBrowser() {
		//step1.set the system path
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\s_mah\\Desktop\\New folder (5)\\chromedriver.exe");
		
	 //step2.create a chrome driver obj
		 driver = new ChromeDriver();
		 
	   //step3.navigate to progressive home page
		 driver.get("https://www.progressive.com/");
		 driver.manage().window().maximize();
		 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "-----was Launched");
	}
	
	 @BeforeMethod
	 public void start() {
		 invokeBrowser();
		 homeP = new HomePage();
		 zipquote = new zipCodePage();
		 nameP = new NameListPage();
		 vehicleP= new VehiclePage();
		 dStatusP = new DriverStatusPage();
		 insuranceP= new InsurancePage();
		 snapSP= new snapShotPage();
	 }
	 
	 @AfterMethod
	 public void end() {
		//driver.close();	 
	 }
	 
	 
	 }
	


