package GoogleTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class GooglePageTest {
static WebDriver driver;

	
	public GooglePageTest(WebDriver driver2) {
	// TODO Auto-generated constructor stub
}


	@BeforeTest
	public void GoogleTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	
@Test (priority =0)

	// Invoking a Browser
	public static void invokeBrowser() {
		driver.navigate().to("http://www.google.com");
		driver.manage().window().maximize();
		driver.navigate().refresh();

		System.out.println(driver.getTitle() + ".......Launched");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

}
	
	@Test (priority = 1)
	public static void actionPerform() {
		//invokeBrowser();
		
	GooglePageTest obj = new GooglePageTest(driver);
	obj.GoogleSearch("What is Selenium");
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

}
	



	@AfterTest
	public static void closeTest() {
		// close browser
		driver.close();
		driver.quit();
		System.out.println("Test Completed");

	}

}






